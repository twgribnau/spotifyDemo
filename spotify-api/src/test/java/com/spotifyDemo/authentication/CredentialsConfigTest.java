package com.spotifyDemo.authentication;

import com.spotifyDemo.SpotifyDemoApplication;
import com.spotifyDemo.controller.SpotifyController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class CredentialsConfigTest {

    @Autowired
    private CredentialsConfig credentialsConfig;

    @Test
    void loadApplicationPropertiesTest() {
        assertEquals("testId", credentialsConfig.getId());
        assertEquals("testSecret", credentialsConfig.getSecret());
    }
}
