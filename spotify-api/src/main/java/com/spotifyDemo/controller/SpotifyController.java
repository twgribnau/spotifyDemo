package com.spotifyDemo.controller;

import com.spotifyDemo.authentication.CredentialsConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpotifyController {

    @Autowired
    CredentialsConfig credentialsConfig;

    @RequestMapping("/hello")
    public String index() {
        return "Greetings from Spring Boot!";
    }


}
