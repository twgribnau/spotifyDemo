package com.spotifyDemo;

import com.spotifyDemo.authentication.CredentialsConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@EnableConfigurationProperties(CredentialsConfig.class)
public class SpotifyDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpotifyDemoApplication.class, args);
    }
}
